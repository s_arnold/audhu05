package lab;

/**
 * Aufgabe H1a)
 * 
 * Abgabe von: <name>, <name> und <name>
 */

import frame.TreeNode;
import frame.TreeNode.NodeColor;

/**
 * An implementation of a Black-Red-Tree
 */
public class RedBlackTree {

	private TreeNode _root;
	private TreeNode _nil;
	private TreeNode _sent = new TreeNode();

	public RedBlackTree() {
		_nil = new TreeNode();
		_root = _nil;
	}

	public TreeNode root() {
		return this._root;
	}

	public TreeNode nil() {
		return this._nil;
	}

	/**
	 * Return the node with the given key, or nil, if no such node exists.
	 */
	public TreeNode search(int key) {
		return searchRec(key, _root);
	}

	public TreeNode searchRec(int key, TreeNode node) {
		TreeNode tmp = node;
		if (_root == _nil) {
			return _nil;
		}
		if (tmp.key > key) {
			if (tmp.left != _nil) {
				return searchRec(key, node.left);
			}
		} else if (tmp.key < key) {
			if (tmp.right != _nil) {
				return searchRec(key, node.right);
			}
		} else if (tmp.key == key){
			return tmp;
		}
		return _nil;

	}

	/**
	 * 
	 * @param sent
	 * @return
	 */
	private void initSent() {
		_root.p = _sent;
		_sent.left = _sent;
		_sent.right = _sent;
		_sent.p = _sent;
		_sent.color = NodeColor.BLACK;
		_sent.key = 0;
		_sent.value = null;
		//return _sent;
	}

	/**
	 * Return the node with the smallest key
	 */
	public TreeNode minimum() {
		return minimumSubtree(_root);
	}

	/**
	 * Return the node with the smallest key in the subtree that has x as root node.
	 */
	public TreeNode minimumSubtree(TreeNode x) {
		// TODO: Implement
		TreeNode node = x;
		TreeNode lowest = x;
		while (node.left != _nil) {
			lowest = node;
			node = node.left;
		}
		return lowest;
	}

	/**
	 * Return the node with the greatest key.
	 */
	public TreeNode maximum() {
		return maximumSubtree(_root);
	}

	/**
	 * Return the node with the greatest key in the subtree that has x as root node.
	 */
	public TreeNode maximumSubtree(TreeNode x) {
		// TODO: Implement
		TreeNode node = x;
		TreeNode highest = x;
		while (node.right != _nil) {
			highest = node;
			node = node.right;
		}
		return highest;
	}

	/**
	 * Insert newNode into the tree.
	 */
	public void insert(TreeNode newNode) {
		//TODO: Implement
		System.out.println(newNode.key);
		
		initSent();
		TreeNode x = _root;
		TreeNode px = _sent;
		
		while (x != _nil) {
			px = x;
			if (x.key > newNode.key) {
				x = x.left;
			} else {
				x = x.right;
			}
		}
		newNode.p = px;
		if (px == _sent) {
			_root = newNode;
		} else {
			if (px.key > newNode.key) {
				px.left = newNode;
			} else {
				px.right = newNode;
			}
		}
		
		newNode.right = _nil;
		newNode.left = _nil;
		newNode.color = NodeColor.RED;
		fixColorsAfterInsertion(newNode);
		
		
	}

	/**
	 * 
	 * @param x
	 */
	private void rotateLeft(TreeNode x) {
		TreeNode y = x.right;
		x.right = y.left;
		if (y.left != _nil) {
			y.left.p = x;
		}
		y.p = x.p;
		if (x.p == _sent) {
			_root = y;
		} else {
			if (x == x.p.left) {
				x.p.left = y;
			} else {
				x.p.right = y;
			}
		}
		y.left = x;
		x.p = y;
	}

	/**
	 * 
	 * @param x
	 */
	private void rotateRight(TreeNode x) {
		TreeNode y = x.left;
		x.left = y.right;
		if (y.right != _nil) {
			y.right.p = x;
		}
		y.p = x.p;
		if (x.p == _sent) {
			_root = y;
		} else {
			if (x == x.p.right) {
				x.p.right = y;
			} else {
				x.p.left = y;
			}
		}
		y.right = x;
		x.p = y;
	}

	/**
	 * 
	 * @param x
	 */
	private void fixColorsAfterInsertion(TreeNode x) {

		while (x.p.color == NodeColor.RED) {
			if (x.p == x.p.p.left) {
				TreeNode y = x.p.p.right;
				
				if (y != _nil && y.color == NodeColor.RED) {
					x.p.color = NodeColor.BLACK;
					y.color = NodeColor.RED;
					x.p.p.color = NodeColor.BLACK;
					x = x.p.p;
				} else {
					if (x == x.p.right) {
						x = x.p;
						rotateLeft(x);
					}
					x.p.color = NodeColor.BLACK;
					x.p.p.color = NodeColor.RED;
					rotateRight(x.p.p);
				}
			} else {
				TreeNode y = x.p.p.left;
				if (y != _nil && y.color == NodeColor.RED) {
					x.p.color = NodeColor.BLACK;
					y.color = NodeColor.RED;
					x.p.p.color = NodeColor.BLACK;
					x = x.p.p;
				} else {
					if (x == x.p.left) {
						x = x.p;
						rotateRight(x);
					}
					x.p.color = NodeColor.BLACK;
					x.p.p.color = NodeColor.RED;
					rotateLeft(x.p.p);
				}
			}
		}
		_root.color = NodeColor.BLACK;
	}

	private void transplant(TreeNode target, TreeNode with) {
		if (target.p == _nil) {
			_root = with;
		} else if (target == target.p.left) {
			target.p.left = with;
		} else {
			target.p.right = with;
		}
		with.p = target.p;
	}

	/**
	 * Delete node toDelete from the tree.
	 */
	public void delete(TreeNode toDelete) {
		if (search(toDelete.key) != _nil) {
			TreeNode x;
			TreeNode y = toDelete;
			NodeColor clr = y.color;

			if (toDelete.left == _nil) {  
				x = toDelete.right;
				transplant(toDelete, toDelete.right); // mit parents von den Knoten versuchen
			} else if (toDelete.right == _nil) {
				x = toDelete.left;
				transplant(toDelete, toDelete.left);
			} else {
				y = minimumSubtree(toDelete.right);
				clr = y.color;
				x = y.right;
				if (y.p == toDelete) {
					x.p = y;
				}
				else {
					transplant(y, y.right);
					y.right = toDelete.right;
					y.right.p = y;
				}
				transplant(toDelete, y);
				y.left = toDelete.left;
				y.left.p = y;
				y.color = toDelete.color;
			}
			if (clr == NodeColor.BLACK) {
				deleteFixup(x);
			}
		}
	}

	
	
	private void deleteFixup(TreeNode x) {
		while (x != _root && x.color == NodeColor.BLACK) {
			if (x == x.p.left) {
				TreeNode w = x.p.right;
				if (w.color == NodeColor.RED) {
					w.color = NodeColor.BLACK;
					x.p.color = NodeColor.RED;
					rotateLeft(x.p);
					w = x.p.right;
				}
				if (w.left.color == NodeColor.BLACK && w.right.color == NodeColor.BLACK) {
					w.color = NodeColor.RED;
					x = x.p;
					//continue;
				} else if (w.right.color == NodeColor.BLACK) {
					w.left.color = NodeColor.BLACK;
					w.color = NodeColor.RED;
					rotateRight(w);
					w = x.p.right;
				}
				if (w.right.color == NodeColor.RED) {
					w.color = x.p.color;
					x.p.color = NodeColor.BLACK;
					w.right.color = NodeColor.BLACK;
					rotateLeft(x.p);
					x = _root;
				}
			} else {
				TreeNode w = x.p.left;
				if (w.color == NodeColor.RED) {
					w.color = NodeColor.BLACK;
					x.p.color = NodeColor.RED;
					rotateRight(x.p);
					w = x.p.left;
				}
				if (w.right.color == NodeColor.BLACK && w.left.color == NodeColor.BLACK) {
					w.color = NodeColor.RED;
					x = x.p;
					//continue;
				} else if (w.left.color == NodeColor.BLACK) {
					w.right.color = NodeColor.BLACK;
					w.color = NodeColor.RED;
					rotateLeft(w);
					w = x.p.left;
				}
				if (w.left.color == NodeColor.RED) {
					w.color = x.p.color;
					x.p.color = NodeColor.BLACK;
					w.left.color = NodeColor.BLACK;
					rotateRight(x.p);
					x = _root;
				}
			}
		}
		x.color = NodeColor.BLACK;
	}
	
	
		public void printTree(TreeNode node) {
	        if (node == _nil) {
	            return;
	        }
	        printTree(node.left);
	        System.out.print(((node.color==NodeColor.RED)?"Color: Red ":"Color: Black ")+"Key: "+node.key+" Parent: "+node.p.key+"\n");
	        printTree(node.right);
	    
	}
}