package lab;

/**
 * Aufgabe H1
 * 
 * Abgabe von: <name>, <name> und <name>
 */

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import frame.TreeNode;
import frame.TreeNodeTestfileReader;

/**
 * Use this class to implement your own tests.
 */
class YourTests {

	@Test
	void test() {
		System.out.println("Starting RBTrees_Test_1...");
		TreeNodeTestfileReader reader = new TreeNodeTestfileReader("tests/public/Testfile_Tree_1");
		ArrayList<TreeNode> treeNodes = reader.readFile();
		RedBlackTree tree = new RedBlackTree();
		
		for (int i = 0; i < 10; i++) {
			tree.insert(treeNodes.get(i));
		}
		
		
		//System.out.println(tree.search(1023).key);
		System.out.println(tree.root().left.left.right.key + " Farbe: " + tree.root().left.left.right.color);
		//tree.printTree(tree.root());
	
		}

}
